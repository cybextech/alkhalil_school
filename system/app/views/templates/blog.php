<section class="content-header">
    <h1>
        <i class="fa fa-pie-chart"></i> {{phrase.Reports}}
    </h1>
</section>

<section class="content" ng-show="views.achievementList">
    <a ng-click="changeView('achievmentPrepare')" class="floatRTL btn btn-danger btn-flat pull-right marginBottom15">{{phrase.Return}}</a>
    <div class="box col-xs-12">
        <div class="box-header">
            <h3 class="box-title">{{phrase.vacationList}}</h3>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-bordered">
                <tbody>

                    <tr>
                        <th></th>
                        <th>{{phrase.user}}</th>
                        <th>{{phrase.Date}}</th>
                        <th>{{phrase.Status}}</th>
                    </tr>
                    <tr>
                        <td>{{report.classId}}</td>
                        <td>{{report.studentSection}}</td>
                        <td>{{report.subjectId}}</td>
                    </tr>
                  
                </tbody>
            </table>
        </div>
    </div>
</section>
<modal visible="showModal"><div ng-bind-html="modalContent"></div></modal>
