<?php

class sectionsController extends \BaseController {

    var $data = array();
    var $panelInit;
    var $layout = 'dashboard';

    public function __construct() {
        $this->panelInit = new \DashboardInit();
        $this->data['panelInit'] = $this->panelInit;
        $this->data['breadcrumb']['Settings'] = \URL::to('/dashboard/languages');
        $this->data['users'] = \Auth::user();
        if ($this->data['users']->role != "admin")
            exit;

        if (!$this->data['users']->hasThePerm('classes')) {
            exit;
        }
    }

    public function listAll() {
        $toReturn = array();

        $classesIn = array();
        $classes = classes::where('classAcademicYear', $this->panelInit->selectAcYear)->get();
        foreach ($classes as $value) {
            $toReturn['classes'][$value->id] = $value->className;
            $classesIn[] = $value->id;
        }

        $toReturn['sections'] = array();
        if (count($classesIn) > 0) {
            $sections = \DB::table('sections')
                    ->select('sections.id as id', 'sections.sectionName as sectionName', 'sections.sectionTitle as sectionTitle', 'sections.classId as classId', 'sections.teacherId as teacherId')
                    ->whereIn('sections.classId', $classesIn)
                    ->get();

            foreach ($sections as $key => $section) {
                $sections[$key]->teacherId = json_decode($sections[$key]->teacherId, true);
                if (isset($toReturn['classes'][$section->classId])) {
                    $toReturn['sections'][$toReturn['classes'][$section->classId]][] = $section;
                }
            }
        }

        $toReturn['teachers'] = array();
        $teachers = User::where('role', 'teacher')->get();
        foreach ($teachers as $value) {
            $toReturn['teachers'][$value->id] = $value->fullName;
             $toReturn['teachers_ed'][]=array('id'=>$value->id,'fullName'=> $value->fullName);
        }

        return $toReturn;
    }

    public function delete($id) {
        if ($postDelete = sections::where('id', $id)->first()) {
            $postDelete->delete();
            return $this->panelInit->apiOutput(true, $this->panelInit->language['delSection'], $this->panelInit->language['sectionDeleted']);
        } else {
            return $this->panelInit->apiOutput(false, $this->panelInit->language['delSection'], $this->panelInit->language['sectionNotExist']);
        }
    }

    public function create() {
        $sections = new sections();
        $sections->sectionName = Input::get('sectionName');
//		$sections->sectionTitle = Input::get('sectionTitle');
        $sections->classId = Input::get('classId');
        $sections->teacherId = json_encode(Input::get('teacherId'));
        $sections->save();

        return $this->panelInit->apiOutput(true, $this->panelInit->language['addSection'], $this->panelInit->language['sectionAdded']);
    }

    function fetch($id) {
        $toReturn = sections::where('id', $id)->first()->toArray();
        $toReturn['teacherId'] = json_decode($toReturn['teacherId'], true);
             $teachers = User::where('role', 'teacher')->get()->toArray();
            $toReturn['teachers'] = array();
        while (list($teacherKey, $teacherValue) = each($teachers)) {
             $toReturn['teachers'][$teacherValue['id']] = $teacherValue;
        }
           while (list($teacherKey, $teacherID) = each($toReturn['teacherId'])) {
                        if (isset($toReturn['teachers'][$teacherID]['fullName'])) {
                            $toReturn['teacherId'][$teacherKey]= array('id'=> $toReturn['teachers'][$teacherID]['id'],
                                                                       'fullName'=>$toReturn['teachers'][$teacherID]['fullName']);
                        } 
                    }
        return $toReturn;
    }

    function edit($id) {
        $sections = sections::find($id);
        $sections->sectionName = Input::get('sectionName');
//		$sections->sectionTitle = Input::get('sectionTitle');
        $sections->classId = Input::get('classId');
        $sections->teacherId = json_encode(Input::get('teacherId'));
        $sections->save();

        return $this->panelInit->apiOutput(true, $this->panelInit->language['editSection'], $this->panelInit->language['sectionUpdated']);
    }

    public function teacherListBySubject($subjectId) {

//        $teachers = User::where('role', 'teacher')->where('subject', $subjectId)->get();
//        $str = "[";
//        $i = 0;
//        $numItems = count($teachers);
//        foreach ($teachers as $value) {
//            $str.='"';
//            $str.=$value->id;
//            $str.='"';
//            if (++$i === $numItems) {
//                $str.="]";
//            } else {
//                $str.=",";
//            }
//            $toReturn['teacherId'] = json_decode($str, true);
//        }
//        //  $toReturn['teacherId'] = json_decode($toReturn['teacherId']);
//        //  $toReturn['teacherId'] =  implode(',', $toReturn['teacherId'] );

        if ($subjectId != 0) {
            if (strpos($subjectId, ',')) {
                $subjectselected = explode(',', $subjectId);
                   $teachers = User::where('role', 'teacher')->whereIn('subject', $subjectselected)->get()->toArray();
            } else {
                $subjectselected = $subjectId;
                   $teachers = User::where('role', 'teacher')->where('subject', $subjectselected)->get()->toArray();
            }
         
            $toReturn['teachers_ed'] = array();
            foreach ($teachers as $teacherValue) {
                $toReturn['teachers_ed'][] = array('id' => $teacherValue['id'], 'fullName' => $teacherValue['fullName']);
                //$toReturn['teachers'][$teacherValue['id']] = $teacherValue['fullName'];
            }
        } else {
            $teachers = User::where('role', 'teacher')->get()->toArray();
            $toReturn['teachers_ed'] = array();
            foreach ($teachers as $teacherValue) {
                $toReturn['teachers_ed'][] = array('id' => $teacherValue['id'], 'fullName' => $teacherValue['fullName']);
                //$toReturn['teachers'][$teacherValue['id']] = $teacherValue['fullName'];
            }
        }
        return $toReturn;
    }

}
