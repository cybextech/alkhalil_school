<?php

class MailSmsController extends \BaseController {

    var $data = array();
    var $panelInit;
    var $layout = 'dashboard';

    public function __construct() {
        $this->panelInit = new \DashboardInit();
        $this->data['panelInit'] = $this->panelInit;
        $this->data['breadcrumb']['Settings'] = \URL::to('/dashboard/languages');
        $this->data['users'] = \Auth::user();
//        if ($this->data['users']->role == "student")
//            exit;

//        if (!$this->data['users']->hasThePerm('mailsms')) {
//            exit;
//        }
    }

    public function listAll() {
        $return = array();
//        $mailsms = mailsms::get()->toArray();
        $mailsms = mailsms::select('mailsms_message.id', 'mailsms.id as mailId', 'mailsms.mailTo', 'mailsms.messageSender', 'mailsms.mailType', 'mailsms_message.message_title', 'mailsms.messageData', 'mailsms.flagreply','mailsms.messageDate')
                        ->leftjoin('mailsms_message', 'mailsms_message.id', '=', 'mailsms.message_id')
                        ->where('mailsms.fromid', '=', $this->data['users']->id)
                        ->get()->toArray();
        foreach ($mailsms as $value) {
                  $value['messageData']= htmlspecialchars_decode($value['messageData'], ENT_QUOTES);
                  $value['session'] = $this->data['users']->role;
           // $value['messageData'] = $value['messageData'];
            if (strlen($value['mailTo']) > 10) {
                $value['mailTo'] = "Custom users list";
            }
            $return[] = $value;
        }
        return $return;
    }

    public function inbox() {
        $return = array();
        $mailsms = mailsms::select('mailsms_message.id', 'mailsms.mailTo', 'mailsms.id as mailId', 'mailsms.messageSender', 'mailsms.mailType', 'mailsms_message.message_title', 'mailsms.messageData', 'mailsms.fromid', 'mailsms.flagreply','mailsms.messageDate')
                        ->leftjoin('mailsms_message', 'mailsms_message.id', '=', 'mailsms.message_id')
                        ->where('mailsms.mailTo', 'like', '%' . $this->data['users']->id . '%')
                        ->get()->toArray();
        foreach ($mailsms as $value) {
            $value['messageData']= htmlspecialchars_decode($value['messageData'], ENT_QUOTES);

            $value['session'] = $this->data['users']->role;
            if (strlen($value['mailTo']) > 10) {
                $value['mailTo'] = "Custom users list";
            }
            $return[] = $value;
        }
        // $return['session_role'] = 
        return $return;
    }

    public function create() {
        $message_title = Input::get('messageTitle');
        $data = [ 'message_title' => $message_title, 'userId' => $this->data['users']->id];
        $toReturnId = DB::table('mailsms_message')->insertGetId($data);
        $mailsms = new mailsms();
        if (Input::get('userType') == "users") {
            $mailsms->mailTo = json_encode(Input::get('selectedUsers'));
        } else {
            if (Input::get('userType') == "teachers") {
                if (Input::get('subjectId') == 0) {
                    $sedList = User::where('role', 'teacher')->get();
                    foreach ($sedList as $user) {
                        $usersList[] = $user['id'];
                    }
                    $mailsms->mailTo = json_encode($usersList);
                } else {
                    $mailsms->mailTo = json_encode(Input::get('classTeacher'));
                }
            } elseif (Input::get('userType') == "students") {
                if (Input::get('classId') == 0) {
                    $sedList = User::where('role', 'student')->get();
                    foreach ($sedList as $user) {
                        $usersList[] = $user['id'];
                    }
                    $mailsms->mailTo = json_encode($usersList);
                } else {
                    if (!Input::get('sectionId') || Input::get('sectionId') == "" || Input::get('sectionId') == "0") {
                        $sedList = User::where('role', 'student')
                                        ->where('studentClass', Input::get('classId'))
                                        ->get()->toarray();
                        foreach ($sedList as $user) {
                            $usersList[] = $user['id'];
                        }
                        $mailsms->mailTo = json_encode($usersList);
                    } else {
                        $mailsms->mailTo = json_encode(Input::get('student'));
                    }
                }
            } elseif (Input::get('userType') == "parents") {
                if (Input::get('classId') == 0) {
                    $sedList = User::where('role', 'student')->get();
                } else {
                    if (!Input::get('sectionId') || Input::get('sectionId') == "" || Input::get('sectionId') == "0") {
                        $sedList = User::where('role', 'student')
                                        ->where('studentClass', Input::get('classId'))
                                        ->get()->toarray();
                        foreach ($sedList as $user) {
                            $usersList[] = $user['id'];
                        }
                    } else {
                        $sedList = Input::get('student');
                        foreach ($sedList as $user) {
                            $usersList[] = $user;
                        }
                    }
                }

                $parent = User::where('role', 'parent')->get();
                $parentsList = array();
                foreach ($parent as $parentValue) {
                    $parentOf = json_decode($parentValue['parentOf'], true);
                    if (is_array($parentOf)) {
                        while (list($key, $value) = each($parentOf)) {
                            if (in_array($value['id'], $usersList)) {
                                $parentsList[] = $parentValue['id'];
                            }
                        }
                    }
                }
                $mailsms->mailTo = json_encode($parentsList);
            }
        }
        $mailsms->mailType = Input::get('sendForm');
        $mailsms->fromid = $this->data['users']->id;
        $mailsms->userid = $this->data['users']->id;
        $mailsms->message_id = $toReturnId;
        $messageData = " ";

        if (Input::get('flagreply') != "") {
            $mailsms->flagreply = Input::get('flagreply');
        }
        if (Input::get('messageTitle') != "") {
            $messageData .= Input::get('messageTitle');
        }
        if (Input::get('messageContent') != "") {
            $messageData .= ' '.Input::get('messageContent');
        }
          $mailsms->messageData = htmlspecialchars($messageData, ENT_QUOTES);

        $mailsms->messageDate = date("F j, Y, g:i a");
        $mailsms->messageSender = $this->data['users']->fullName;
        $mailsms->save();

//        if (Input::get('userType') == "teachers") {
//            $sedList = User::where('role', 'teacher')->get();
//        }
//        if (Input::get('userType') == "students") {
//            if (!Input::get('classId') || Input::get('classId') == "" || Input::get('classId') == "0") {
//                $sedList = User::where('role', 'student')->get();
//            } else {
//                $sedList = User::where('role', 'student')->where('studentClass', Input::get('classId'))->get();
//            }
//        }
//        if (Input::get('userType') == "parents") {
//            $sedList = User::where('role', 'parent')->get();
//        }
//        if (Input::get('userType') == "users") {
//            $usersList = array();
//            $selectedUsers = Input::get('selectedUsers');
//            foreach ($selectedUsers as $user) {
//                $usersList[] = $user['id'];
//            }
//
//            $sedList = User::whereIn('id', $usersList)->get();
//        }
//        $SmsHandler = new MailSmsHandler();
//
//        if (Input::get('sendForm') == "email") {
//            foreach ($sedList as $user) {
//                $SmsHandler->mail($user->email, Input::get('messageTitle'), Input::get('messageContent'), $user->fullName);
//            }
//        }
//
//        if (Input::get('sendForm') == "sms") {
//            foreach ($sedList as $user) {
//                if ($user->mobileNo != "") {
//                    $SmsHandler->sms($user->mobileNo, strip_tags(Input::get('messageContent')));
//                }
//            }
//        }
        $mailsms->messageDate = htmlspecialchars_decode($mailsms->messageDate);
        return $this->listAll();
    }

    public function reply($id, $toReturnId) {

        $mailsms = new mailsms();
        $mailsms->mailType = 'email';
        $mailsms->fromid = $this->data['users']->id;
        $mailsms->userid = $this->data['users']->id;
        // $userlist[]=$toReturnId;
        $mailsms->message_id = $toReturnId;
        $mailsms->mailTo = json_encode($id);
        $messageData = " ";
        if (Input::get('messageContent') != "") {
            $messageData .= ' '.Input::get('messageContent');
        }
        $mailsms->messageData = htmlspecialchars($messageData, ENT_QUOTES);
        if (Input::get('flagreply') != "") {
            $mailsms->flagreply = Input::get('flagreply');
        }
        $mailsms->messageDate = date("F j, Y, g:i a");
        $mailsms->messageSender = $this->data['users']->fullName;
        $mailsms->save();
        $mailsms->messageDate = htmlspecialchars_decode($mailsms->messageDate);
        return $this->listAll();
    }

    public function settings() {
        $toReturn = array();
        $toReturn['sms'] = json_decode($this->panelInit->settingsArray['smsProvider']);
        $toReturn['mail'] = json_decode($this->panelInit->settingsArray['mailProvider']);
        return $toReturn;
    }

    public function settingsSave() {
        if (Input::get('mailProvider')) {
            $settings = settings::where('fieldName', 'mailProvider')->first();
            $settings->fieldValue = json_encode(Input::all());
            $settings->save();
        } else {
            $settings = settings::where('fieldName', 'smsProvider')->first();
            $settings->fieldValue = json_encode(Input::all());
            $settings->save();
        }

        return $this->panelInit->apiOutput(true, $this->panelInit->language['saveSettings'], $this->panelInit->language['settSaved']);
    }

    public function teacherListBySubject($subjectId) {
        if ($subjectId != 0) {
            $toReturn = User::where('role', 'teacher')->where('subject', $subjectId)->get()->toArray();
        } else {
            $toReturn = User::where('role', 'teacher')->get()->toArray();
        }

        return $toReturn;
    }
    public function delete($id) {
        if ($this->data['users']->role == "student" || $this->data['users']->role == "parent")
            exit;
        if ($postDelete = mailsms::where('id', $id)->first()) {
            $postDelete->delete();
            return $this->panelInit->apiOutput(true, NULL, NULL);
        } else {
            return $this->panelInit->apiOutput(false, NULL, NULL);
        }
    }

    public function studentListByclasssubject($classId, $sectionId) {
        if ($classId == 0) {
            $toReturn = User::where('role', 'student')->get()->toArray();
        } else {
            if ($sectionId != 0) {
                $toReturn = User::where('role', 'student')->where('studentClass', $classId)
                                ->where('studentSection', $sectionId)->get()->toArray();
            } else {
                $toReturn = User::where('role', 'student')->where('studentClass', $classId)
                                ->get()->toArray();
            }
        }

        return $toReturn;
    }

}
