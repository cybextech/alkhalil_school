<?php

class LibraryController extends \BaseController {

    var $data = array();
    var $panelInit;
    var $layout = 'dashboard';

    public function __construct() {
        $this->panelInit = new \DashboardInit();
        $this->data['panelInit'] = $this->panelInit;
        $this->data['breadcrumb']['Settings'] = \URL::to('/dashboard/languages');
        $this->data['users'] = \Auth::user();

        if (!$this->data['users']->hasThePerm('Library')) {
            exit;
        }
    }

    public function listAll($page = 1) {
        $toReturn = array();
        if ($this->data['users']->role == "teacher") {
            $toReturn['bookLibrary'] = \DB::table('book_library')
                    ->leftJoin('subject', 'subject.id', '=', 'book_library.booksubject')
                    ->leftJoin('classes', 'classes.id', '=', 'book_library.bookgrade')
                    ->select('book_library.id', 'book_library.bookName', 'subject.subjectTitle', 'book_library.bookgrade', 'book_library.bookDescription', 'book_library.bookAuthor', 'book_library.bookType', 'book_library.bookFile', 'book_library.booklink', 'book_library.bookState', 'book_library.bookPrice', 'classes.className')
                    ->where('book_library.booksubject', $this->data['users']->subject)
                    ->get();
            // $toReturn['bookLibrary'] = book_library::where("booksubject", $this->data['users']->subject)->orderBy('id', 'DESC')->take('20')->skip(20 * ($page - 1))->get()->toArray();
            // $toReturn['totalItems'] = book_library::count();
            $toReturn['totalItems'] = count($toReturn['bookLibrary']);
        } else if ($this->data['users']->role == "student") {
            //$toReturn['bookLibrary'] = book_library::where("bookgrade", $this->data['users']->studentClass)->orderBy('id', 'DESC')->take('20')->skip(20 * ($page - 1))->get()->toArray();
            // $toReturn['totalItems'] = book_library::count(); 
            $toReturn['bookLibrary'] = \DB::table('book_library')
                    ->leftJoin('subject', 'subject.id', '=', 'book_library.booksubject')
                    ->leftJoin('classes', 'classes.id', '=', 'book_library.bookgrade')
                    ->select('book_library.id', 'book_library.bookName', 'subject.subjectTitle', 'book_library.bookgrade', 'book_library.bookDescription', 'book_library.bookAuthor', 'book_library.bookType', 'book_library.bookFile', 'book_library.booklink', 'book_library.bookState', 'book_library.bookPrice', 'classes.className')
                    ->where('book_library.bookgrade', $this->data['users']->studentClass)
                    ->get();
            $toReturn['totalItems'] = count($toReturn['bookLibrary']);
        } else if ($this->data['users']->role == "parent") {
            $parentOf = json_decode($this->data['users']->parentOf, true);
            if (is_array($parentOf)) {
                while (list($key, $value) = each($parentOf)) {
                    $studentId[] = $value['id'];
                }
            }

            if (count($studentId) > 0) {
                $gradeStudent = User::whereIN("id", $studentId)->get()->toarray();
                foreach ($gradeStudent as $grade) {
                    $student[] = $grade['studentClass'];
                    $toReturn['student_parent'][] = array(
                        'grade_id' => $grade['studentClass'],
                        'fullname' => $grade['fullName']);
                }
                // $toReturn['student_parent'] = $student_arr;
//                $toReturn['bookLibrary'] = book_library::whereIn('bookgrade', $student)->orderBy('id', 'DESC')->take('20')->skip(20 * ($page - 1))->get()->toArray();
//                $toReturn['totalItems'] = book_library::count();
                $toReturn['bookLibrary'] = \DB::table('book_library')
                        ->leftJoin('subject', 'subject.id', '=', 'book_library.booksubject')
                        ->leftJoin('classes', 'classes.id', '=', 'book_library.bookgrade')
                        ->select('book_library.id', 'book_library.bookName', 'subject.subjectTitle', 'book_library.bookgrade', 'book_library.bookDescription', 'book_library.bookAuthor', 'book_library.bookType', 'book_library.bookFile', 'book_library.booklink', 'book_library.bookState', 'book_library.bookPrice', 'classes.className')
                        ->whereIn('book_library.bookgrade', $student)
                        ->get();
                $toReturn['totalItems'] = count($toReturn['bookLibrary']);
            } else {
                $toReturn['bookLibrary'] = array();
                $toReturn['totalItems'] = 0;
                $toReturn['student_parent'] = array();
            }
//            $toReturn['bookLibrary'] = book_library::where("bookgrade", $this->data['users']->studentClass)->orderBy('id', 'DESC')->take('20')->skip(20 * ($page - 1))->get()->toArray();
//            $toReturn['totalItems'] = book_library::count();
        } else {
//            $toReturn['bookLibrary'] = book_library::orderBy('id', 'DESC')->take('20')->skip(20 * ($page - 1))->get()->toArray();
//            $toReturn['totalItems'] = book_library::count();
            $toReturn['bookLibrary'] = \DB::table('book_library')
                            ->leftJoin('subject', 'subject.id', '=', 'book_library.booksubject')
                            ->leftJoin('classes', 'classes.id', '=', 'book_library.bookgrade')
                            ->select('book_library.id', 'book_library.bookName', 'subject.subjectTitle', 'book_library.bookgrade', 'book_library.bookDescription', 'book_library.bookAuthor', 'book_library.bookType', 'book_library.bookFile', 'book_library.booklink', 'book_library.bookState', 'book_library.bookPrice', 'classes.className')->get();
            $toReturn['totalItems'] = count($toReturn['bookLibrary']);
        }
        $toReturn['subjects'] = subject::get()->toArray();
        $toReturn['userRole'] = $this->data['users']->role;

        return $toReturn;
    }

    public function search($keyword, $student_parent, $page = 1) {
        $toReturn = array();
//        if ($this->data['users']->role == "teacher") {
//            $toReturn['bookLibrary'] = book_library::where("booksubject", $this->data['users']->subject)->where('bookName', 'like', '%' . $keyword . '%')->orWhere('bookDescription', 'like', '%' . $keyword . '%')->orWhere('bookAuthor', 'like', '%' . $keyword . '%')->orderBy('id', 'DESC')->take('20')->skip(20 * ($page - 1))->get()->toArray();
//            $toReturn['totalItems'] = book_library::where("booksubject", $this->data['users']->subject)->where('bookName', 'like', '%' . $keyword . '%')->orWhere('bookDescription', 'like', '%' . $keyword . '%')->orWhere('bookAuthor', 'like', '%' . $keyword . '%')->count();
//        } else {
//
//            $toReturn['bookLibrary'] = book_library::where('bookName', 'like', '%' . $keyword . '%')->orWhere('bookDescription', 'like', '%' . $keyword . '%')->orWhere('bookAuthor', 'like', '%' . $keyword . '%')->orderBy('id', 'DESC')->take('20')->skip(20 * ($page - 1))->get()->toArray();
//            $toReturn['totalItems'] = book_library::where('bookName', 'like', '%' . $keyword . '%')->orWhere('bookDescription', 'like', '%' . $keyword . '%')->orWhere('bookAuthor', 'like', '%' . $keyword . '%')->count();
//        }
//
//        return $toReturn;
        if ($this->data['users']->role == "teacher") {
            $toReturn['bookLibrary'] = \DB::table('book_library')
                    ->leftJoin('subject', 'subject.id', '=', 'book_library.booksubject')
                    ->select('book_library.id', 'book_library.bookName', 'subject.subjectTitle', 'book_library.bookgrade', 'book_library.bookDescription', 'book_library.bookAuthor', 'book_library.bookType', 'book_library.bookFile', 'book_library.booklink', 'book_library.bookState', 'book_library.bookPrice')
                    ->where('book_library.booksubject', $this->data['users']->subject)
                    ->orwhere('book_library.bookName', 'like', '%' . $keyword . '%')
                    ->orwhere('book_library.booksubject', 'like', '%' . $keyword . '%')
                    ->orWhere('book_library.bookDescription', 'like', '%' . $keyword . '%')
                    ->orWhere('subject.subjectTitle', 'like', '%' . $keyword . '%')
                    ->get();
            $toReturn['totalItems'] = count($toReturn['bookLibrary']);
//            $toReturn['bookLibrary'] = book_library::where("booksubject", $this->data['users']->subject)->where('bookName', 'like', '%' . $keyword . '%')->orWhere('bookDescription', 'like', '%' . $keyword . '%')->orWhere('bookAuthor', 'like', '%' . $keyword . '%')->orderBy('id', 'DESC')->take('20')->skip(20 * ($page - 1))->get()->toArray();
//            $toReturn['totalItems'] = book_library::where("booksubject", $this->data['users']->subject)->where('bookName', 'like', '%' . $keyword . '%')->orWhere('bookDescription', 'like', '%' . $keyword . '%')->orWhere('bookAuthor', 'like', '%' . $keyword . '%')->count();
        } else if ($this->data['users']->role == "student") {
            $toReturn['bookLibrary'] = \DB::table('book_library')
                    ->leftJoin('subject', 'subject.id', '=', 'book_library.booksubject')
                    ->select('book_library.id', 'book_library.bookName', 'subject.subjectTitle', 'book_library.bookgrade', 'book_library.bookDescription', 'book_library.bookAuthor', 'book_library.bookType', 'book_library.bookFile', 'book_library.booklink', 'book_library.bookState', 'book_library.bookPrice')
                    ->where('book_library.bookgrade', $this->data['users']->studentClass)
                    ->orwhere('book_library.bookName', 'like', '%' . $keyword . '%')
                    ->orwhere('subject.subjectTitle', 'like', '%' . $keyword . '%')
                    ->orWhere('book_library.bookDescription', 'like', '%' . $keyword . '%')
                    ->orWhere('book_library.bookAuthor', 'like', '%' . $keyword . '%')
                    ->get();
            $toReturn['totalItems'] = count($toReturn['bookLibrary']);
//            $toReturn['bookLibrary'] = book_library::where("booksubject", $this->data['users']->subject)->where('bookName', 'like', '%' . $keyword . '%')->orWhere('bookDescription', 'like', '%' . $keyword . '%')->orWhere('bookAuthor', 'like', '%' . $keyword . '%')->orderBy('id', 'DESC')->take('20')->skip(20 * ($page - 1))->get()->toArray();
//            $toReturn['totalItems'] = book_library::where("booksubject", $this->data['users']->subject)->where('bookName', 'like', '%' . $keyword . '%')->orWhere('bookDescription', 'like', '%' . $keyword . '%')->orWhere('bookAuthor', 'like', '%' . $keyword . '%')->count();
        } else if ($this->data['users']->role == "parent") {
            $parentOf = json_decode($this->data['users']->parentOf, true);
            if (is_array($parentOf)) {
                while (list($key, $value) = each($parentOf)) {
                    $studentId[] = $value['id'];
                }
            }
            $gradeStudent = User::whereIN("id", $studentId)->get()->toarray();
            foreach ($gradeStudent as $grade) {
                $student[] = $grade['studentClass'];
                $toReturn['student_parent'][] = array(
                    'grade_id' => $grade['studentClass'],
                    'fullname' => $grade['fullName']);
            }
            $bookLibrary = book_library::select('book_library.id', 'book_library.bookName', 'subject.subjectTitle', 'book_library.bookgrade', 'book_library.bookDescription', 'book_library.bookAuthor', 'book_library.bookType', 'book_library.bookFile', 'book_library.booklink', 'book_library.bookState', 'book_library.bookPrice')
                    ->leftJoin('subject', 'subject.id', '=', 'book_library.booksubject')
                    ->whereIn('bookgrade', $student);
            if (isset($keyword) AND $keyword != "" AND $keyword != "undefined") {
                $bookLibrary->orwhere('book_library.bookName', 'like', '%' . $keyword . '%')
                        ->orWhere('book_library.bookDescription', 'like', '%' . $keyword . '%')
                        ->orWhere('book_library.bookAuthor', 'like', '%' . $keyword . '%')
                        ->orwhere('subject.subjectTitle', 'like', '%' . $keyword . '%');
            }

            if (isset($student_parent) AND $student_parent != "" AND $student_parent != "undefined" AND $student_parent != 0) {
                $bookLibrary = $bookLibrary->where('bookgrade', $student_parent);
            }
            $toReturn['totalItems'] = $bookLibrary->count();
            //$bookLibrary = $bookLibrary->orderBy('id', 'DESC')->take('20')->skip(20 * ($page - 1))->get()->toArray();
            $bookLibrary = $bookLibrary->get();
            $toReturn['bookLibrary'] = $bookLibrary;

//        while (list(, $bookLib) = each($bookLibrary)) {
//            $toReturn['bookLibrary'][] = array('id' => $bookLib['id'], "bookName" => $bookLib['bookName'], "bookAuthor" => $bookLib['bookAuthor'], "username" => $bookLib['username'], "bookState" => $bookLib['bookState'], "booklink" => $bookLib['booklink']);
//        }
        } else {
            $toReturn['bookLibrary'] = book_library::select('book_library.id', 'book_library.bookName', 'subject.subjectTitle', 'book_library.bookgrade', 'book_library.bookDescription', 'book_library.bookAuthor', 'book_library.bookType', 'book_library.bookFile', 'book_library.booklink', 'book_library.bookState', 'book_library.bookPrice')
                            ->leftJoin('subject', 'subject.id', '=', 'book_library.booksubject')
                            ->where('book_library.bookName', 'like', '%' . $keyword . '%')
                            ->orWhere('book_library.bookDescription', 'like', '%' . $keyword . '%')
                            ->orwhere('subject.subjectTitle', 'like', '%' . $keyword . '%')
                            ->orWhere('book_library.bookAuthor', 'like', '%' . $keyword . '%')
                            ->orderBy('id', 'DESC')->take('20')->skip(20 * ($page - 1))->get()->toArray();
            $toReturn['totalItems'] = book_library::select('book_library.id', 'book_library.bookName', 'subject.subjectTitle', 'book_library.bookgrade', 'book_library.bookDescription', 'book_library.bookAuthor', 'book_library.bookType', 'book_library.bookFile', 'book_library.booklink', 'book_library.bookState', 'book_library.bookPrice')
                            ->leftJoin('subject', 'subject.id', '=', 'book_library.booksubject')
                            ->where('book_library.bookName', 'like', '%' . $keyword . '%')
                            ->orWhere('book_library.bookDescription', 'like', '%' . $keyword . '%')
                            ->orwhere('subject.subjectTitle', 'like', '%' . $keyword . '%')
                            ->orWhere('book_library.bookAuthor', 'like', '%' . $keyword . '%')->count();
        }
        return $toReturn;
    }

    public function delete($id) {
        if ($this->data['users']->role != "admin")
            exit;
        if ($postDelete = book_library::where('id', $id)->first()) {
            @unlink('uploads/books/' . $postDelete->bookFile);
            $postDelete->delete();
            return $this->panelInit->apiOutput(true, $this->panelInit->language['delLibrary'], $this->panelInit->language['itemdel']);
        } else {
            return $this->panelInit->apiOutput(false, $this->panelInit->language['delLibrary'], $this->panelInit->language['itemNotExist']);
        }
    }

    public function download($id) {
        $toReturn = book_library::where('id', $id)->first();
        if (file_exists('uploads/books/' . $toReturn->bookFile)) {
            $fileName = preg_replace('/[^a-zA-Z0-9-_\.]/', '-', $toReturn->bookName) . "." . pathinfo($toReturn->bookFile, PATHINFO_EXTENSION);
            header("Content-Type: application/force-download");
            header("Content-Disposition: attachment; filename=" . $fileName);
            echo file_get_contents('uploads/books/' . $toReturn->bookFile);
        }
        exit;
    }

    public function create() {
        if ($this->data['users']->role != "admin")
            exit;
        $bookLibrary = new book_library();
        $bookLibrary->bookgrade = Input::get('classId');
        $bookLibrary->booksubject = Input::get('subjectId');
        $bookLibrary->bookName = Input::get('bookName');
        $bookLibrary->bookDescription = Input::get('bookDescription');
        $bookLibrary->bookAuthor = $this->data['users']->fullName;
        $bookLibrary->bookType = Input::get('bookType');
        //$bookLibrary->bookPrice = Input::get('bookPrice');
        $bookLibrary->bookState = Input::get('bookState');
        $bookLibrary->booklink = Input::get('link');
        $bookLibrary->save();

        if (Input::hasFile('bookFile')) {
            $fileInstance = Input::file('bookFile');
            $newFileName = "book_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
            $fileInstance->move('uploads/books/', $newFileName);

            $bookLibrary->bookFile = $newFileName;
            $bookLibrary->save();
        }

        return $this->panelInit->apiOutput(true, $this->panelInit->language['addBook'], $this->panelInit->language['bookAdded'], $bookLibrary->toArray());
    }

    function fetch($id) {
      //  if ($this->data['users']->role == "teacher") {
            $data = book_library::where('id', $id)->first()->toArray();
//        } else {
//            $data = book_library::where('id', $id)->first()->toArray();
//        }
        return json_encode($data);
       // return $data;
    }

    function edit($id) {
        if ($this->data['users']->role != "admin")
            exit;
        $bookLibrary = book_library::find($id);
        $bookLibrary->bookgrade = Input::get('classId');
        $bookLibrary->booksubject = Input::get('subjectId');
        $bookLibrary->bookName = Input::get('bookName');
        $bookLibrary->bookDescription = Input::get('bookDescription');
        $bookLibrary->bookAuthor = $this->data['users']->fullName;
        $bookLibrary->bookType = Input::get('bookType');
        //$bookLibrary->bookPrice = Input::get('bookPrice');
        $bookLibrary->bookState = Input::get('bookState');
        $bookLibrary->booklink = Input::get('link');
        if (Input::hasFile('bookFile')) {
            @unlink("uploads/books/" . $bookLibrary->bookFile);
            $fileInstance = Input::file('bookFile');
            $newFileName = "book_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
            $fileInstance->move('uploads/books/', $newFileName);

            $bookLibrary->bookFile = $newFileName;
        }
        $bookLibrary->save();

        return $this->panelInit->apiOutput(true, $this->panelInit->language['editBook'], $this->panelInit->language['bookModified'], $bookLibrary->toArray());
    }

//    public function export() {
//        echo "here";
////        if ($this->data['users']->role != "admin")
////            exit;
////        $data = array(1 => array('Book Title', 'Book Description', 'Grade', 'Subject', 'Book Author', 'Link'));
////        $bookLibrary = \DB::table('book_library')
////                ->leftJoin('subject', 'subject.id', '=', 'book_library.booksubject')
////                ->leftJoin('classes', 'classes.id', '=', 'book_library.bookgrade')
////                ->select('book_library.id', 'book_library.bookName', 'subject.subjectTitle', 'book_library.bookgrade', 'book_library.bookDescription', 'book_library.bookAuthor', 'book_library.bookType', 'book_library.bookFile', 'book_library.booklink', 'book_library.bookState', 'book_library.bookPrice', 'classes.className')
////                ->get();
////        print_r($bookLibrary);die;
////        foreach ($bookLibrary as $value) {
////            $data[] = array($value->bookName, $value->bookDescription, $value->className, $value->subjectTitle, $value->bookAuthor, $value->booklink);
////        }
////        $xls = new Excel_XML('UTF-8', false, 'Library Sheet');
////        $xls->addArray($data);
////        $xls->generateXML('Library-Sheet');
////        exit;
//    }

}
