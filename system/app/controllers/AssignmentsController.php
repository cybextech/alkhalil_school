<?php

class AssignmentsController extends \BaseController {

    var $data = array();
    var $panelInit;
    var $layout = 'dashboard';

    public function __construct() {
        $this->panelInit = new \DashboardInit();
        $this->data['panelInit'] = $this->panelInit;
        $this->data['breadcrumb']['Settings'] = \URL::to('/dashboard/languages');
        $this->data['users'] = \Auth::user();

        if (!$this->data['users']->hasThePerm('Assignments')) {
            exit;
        }
    }

    public function listAll() {
        DB::table('mob_notifications')
                ->where('type_notify', 'assignment')
                ->where('read_notify', '0')
                ->where('notifToIds', $this->data['users']->id)
                ->update(['read_notify' => '1']);
        $toReturn = array();
        if ($this->data['users']->role == "teacher") {
            $toReturn['classes'] = classes::where('classAcademicYear', $this->panelInit->selectAcYear)
                            ->where('classTeacher', 'LIKE', '%"' . $this->data['users']->id . '"%')->get()->toArray();
            $subjects = subject::where('id', $this->data['users']->subject)->get()->toArray();
            $subjectArray = array();
            while (list(, $subject) = each($subjects)) {
                $subjectArray[$subject['id']] = $subject['subjectTitle'];
            }
        } else {
            $toReturn['classes'] = classes::where('classAcademicYear', $this->panelInit->selectAcYear)->get()->toArray();
            $subjects = subject::get()->toArray();
            $subjectArray = array();
            while (list(, $subject) = each($subjects)) {
                $subjectArray[$subject['id']] = $subject['subjectTitle'];
            }
        }
        $classesArray = array();
        while (list(, $class) = each($toReturn['classes'])) {
            $classesArray[$class['id']] = $class['className'];
        }
        $sections = sections::get()->toArray();
        $sectionArray = array();
        while (list(, $section) = each($sections)) {
            $sectionArray[$section['id']] = $section['sectionName'];
        }
        $toReturn['assignments'] = array();
        if (count($classesArray) > 0) {
            $assignments = new assignments();
            if ($this->data['users']->role == "teacher") {
                //$assignments = $assignments->where('teacherId', $this->data['users']->id);
                $assignments = $assignments->where('subjectId', $this->data['users']->subject)->where('teacherId', $this->data['users']->id);
            } else if ($this->data['users']->role == "student") {
                $assignments = $assignments->where('classId', 'LIKE', '%"' . $this->data['users']->studentClass . '"%');
                if ($this->panelInit->settingsArray['enableSections'] == true) {
                    $assignments = $assignments->where('sectionId', 'LIKE', '%"' . $this->data['users']->studentSection . '"%');
                }
            } else if ($this->data['users']->role == "admin") {
//                $assignments = $assignments->where(function($query) use ($classesArray) {
//                    while (list($key, ) = each($classesArray)) {
//                        $query = $query->orWhere('classId', 'LIKE', '%"' . $key . '"%');
//                    }
//                });
            }

            $assignments = $assignments->get();

            foreach ($assignments as $key => $assignment) {
                $classId = json_decode($assignment->classId);
                $sectionId = json_decode($assignment->sectionId);
                if ($this->data['users']->role == "student" AND ! in_array($this->data['users']->studentClass, $classId)) {
                    continue;
                }
                $toReturn['assignments'][$key]['id'] = $assignment->id;
                $toReturn['assignments'][$key]['subjectId'] = $assignment->subjectId;
                $toReturn['assignments'][$key]['teacherId'] = $assignment->teacherId;
                $toReturn['assignments'][$key]['subject'] = $subjectArray[$assignment->subjectId];
                $toReturn['assignments'][$key]['AssignTitle'] = $assignment->AssignTitle;
                $toReturn['assignments'][$key]['AssignDescription'] = $assignment->AssignDescription;
                $toReturn['assignments'][$key]['AssignDescription'] = htmlspecialchars_decode($toReturn['assignments'][$key]['AssignDescription'], ENT_QUOTES);
                $toReturn['assignments'][$key]['AssignFile'] = $assignment->AssignFile;
                $toReturn['assignments'][$key]['AssignDeadLine'] = $assignment->AssignDeadLine;
                $toReturn['assignments'][$key]['AssignDate'] = $assignment->AssignDate;
                $toReturn['assignments'][$key]['assignmentAuthor'] = $assignment->assignmentAuthor;
                $toReturn['assignments'][$key]['assignment_link'] = $assignment->assignment_link;
                $toReturn['assignments'][$key]['classes'] = "";
                $toReturn['assignments'][$key]['sections'] = "";
                while (list(, $value) = each($classId)) {
                    if (isset($classesArray[$value])) {
                        $toReturn['assignments'][$key]['classes'] .= $classesArray[$value] . ", ";
                    }
                }

                while (list(, $value) = each($sectionId)) {
                    if (isset($sectionArray[$value])) {
                        $toReturn['assignments'][$key]['sections'] .= $sectionArray[$value] . ", ";
                    }
                }
            }
        }

        $toReturn['userRole'] = $this->data['users']->role;
        $toReturn['id'] = $this->data['users']->id;
        return $toReturn;
    }

    public function download($id) {
        $toReturn = assignments::where('id', $id)->first();
        if (file_exists('uploads/assignments/' . $toReturn->AssignFile)) {
            $fileName = preg_replace('/[^a-zA-Z0-9-_\.]/', '-', $toReturn->AssignTitle) . "." . pathinfo($toReturn->AssignFile, PATHINFO_EXTENSION);
            header("Content-Type: application/force-download");
            header("Content-Disposition: attachment; filename=" . $fileName);
            echo file_get_contents('uploads/assignments/' . $toReturn->AssignFile);
        }
        exit;
    }

    public function delete($id) {
        if ($this->data['users']->role == "student" || $this->data['users']->role == "parent")
            exit;
        if ($postDelete = assignments::where('id', $id)->first()) {
            @unlink("uploads/assignments/" . $postDelete->AssignFile);
            $postDelete->delete();
            return $this->panelInit->apiOutput(true, $this->panelInit->language['delAssignment'], $this->panelInit->language['assignemntDel']);
        } else {
            return $this->panelInit->apiOutput(false, $this->panelInit->language['delAssignment'], $this->panelInit->language['assignemntNotExist']);
        }
    }

    public function create() {
        if ($this->data['users']->role == "student" || $this->data['users']->role == "parent")
            exit;
        $assignments = new assignments();
        $assignments->classId = json_encode(Input::get('classId'));
        //if($this->panelInit->settingsArray['enableSections'] == true){
        $assignments->sectionId = json_encode(Input::get('sectionId'));
        //}
        $assignments->subjectId = Input::get('subjectId');
        $assignments->teacherId = $this->data['users']->id;
        $assignments->AssignTitle = Input::get('AssignTitle');
        $assignments->assignment_link = Input::get('assignment_link');
        $assignments->assignmentAuthor = $this->data['users']->fullName;
        //  $assignments->AssignDescription = Input::get('AssignDescription');
        $assignments->AssignDescription = htmlspecialchars(Input::get('AssignDescription'), ENT_QUOTES);
        $assignments->AssignDeadLine = $this->panelInit->dateToUnix(Input::get('AssignDeadLine'));
        $assignments->AssignDate = $this->panelInit->dateToUnix(Input::get('AssignDate'));
        $assignments->teacherId = $this->data['users']->id;
//        $assignments->save();
        if (Input::hasFile('AssignFile')) {
            $fileInstance = Input::file('AssignFile');
            $newFileName = "assignments_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
            $fileInstance->move('uploads/assignments/', $newFileName);

            $assignments->AssignFile = $newFileName;
            
        }$assignments->save();
        $classes = Input::get('classId');
        $sections = Input::get('sectionId');
//        while (list(, $value) = each($classes)) {
//            $students = User::where('role', 'student')->where('studentClass', $value)->where('activated', '0')->orderBy('studentRollId', 'ASC')->first();
//            if (!empty($students)) {
//                $this->panelInit->mobNotifyUser('class', $students->id, $this->panelInit->language['newAssigmentAdded'] . " " . Input::get('AssignTitle'));
//            }
//        }
        while (list(, $value) = each($classes)) {
            $classesList[] = $value;
        }
        while (list(, $value) = each($sections)) {
            $sectionsList[] = $value;
        }
        $students = User::where('role', 'student')->whereIn('studentClass', $classesList)->whereIn('studentSection', $sectionsList)->get()->toArray();
        if (!empty($students)) {
            foreach ($students as $student) {
                $this->panelInit->mobNotifyUser('class', $student['id'], $this->panelInit->language['newAssigmentAdded'] . " " . Input::get('AssignTitle'), 'assignment');
            }
        }
        $assignments->AssignDescription = htmlspecialchars_decode($assignments->AssignDescription);
        return $this->panelInit->apiOutput(true, $this->panelInit->language['AddAssignments'], $this->panelInit->language['assignmentCreated'], $assignments->toArray());
    }

    function fetch($id) {
        $toReturn = assignments::where('id', $id)->first();
        $toReturn['AssignDescription'] = htmlspecialchars_decode($toReturn['AssignDescription'], ENT_QUOTES);
        $DashboardController = new DashboardController();
        $toReturn['sections'] = $DashboardController->sectionsList(json_decode($toReturn->classId, true));
        $toReturn['subject'] = $DashboardController->subjectList(json_decode($toReturn->classId, true));
        return $toReturn;
    }

    function edit($id) {
        if ($this->data['users']->role == "student" || $this->data['users']->role == "parent")
            exit;
        $assignments = assignments::find($id);
        $assignments->classId = json_encode(Input::get('classId'));
        //if($this->panelInit->settingsArray['enableSections'] == true){
        $assignments->sectionId = json_encode(Input::get('sectionId'));
        //}
        $assignments->subjectId = Input::get('subjectId');
        $assignments->teacherId = $this->data['users']->id;
        $assignments->AssignTitle = Input::get('AssignTitle');
        $assignments->assignment_link = Input::get('assignment_link');
        $assignments->assignmentAuthor = $this->data['users']->fullName;
        //  $assignments->AssignDescription = Input::get('AssignDescription');
        $assignments->AssignDescription = htmlspecialchars(Input::get('AssignDescription'), ENT_QUOTES);
        $assignments->AssignDeadLine = $this->panelInit->dateToUnix(Input::get('AssignDeadLine'));
        $assignments->AssignDate = $this->panelInit->dateToUnix(Input::get('AssignDate'));
        if (Input::hasFile('AssignFile')) {
            @unlink("uploads/assignments/" . $assignments->AssignFile);
            $fileInstance = Input::file('AssignFile');
            $newFileName = "assignments_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
            $fileInstance->move('uploads/assignments/', $newFileName);

            $assignments->AssignFile = $newFileName;
//            $assignments->save();
        }
        $assignments->save();
     
        $assignments->AssignDescription = htmlspecialchars_decode($assignments->AssignDescription);
        return $this->panelInit->apiOutput(true, $this->panelInit->language['editAssignment'], $this->panelInit->language['assignmentModified'], $assignments->toArray());
    }

    function checkUpload() {
        $toReturn = assignments::where('id', Input::get('assignmentId'))->first();

        if ($toReturn->AssignDeadLine < time()) {
            return $this->panelInit->apiOutput(false, $this->panelInit->language['applyAssAnswer'], $this->panelInit->language['assDeadTime']);
        }

        $assignmentsAnswers = assignments_answers::where('assignmentId', Input::get('assignmentId'))->where('userId', $this->data['users']->id)->count();
        if ($assignmentsAnswers > 0) {
            return $this->panelInit->apiOutput(false, $this->panelInit->language['applyAssAnswer'], $this->panelInit->language['assAlreadySub']);
        }
        return array("canApply" => "true");
    }

    function upload($id) {
        if ($this->data['users']->role == "admin" || $this->data['users']->role == "teacher")
            exit;
        $assignmentsAnswers = new assignments_answers();
        $assignmentsAnswers->assignmentId = $id;
        $assignmentsAnswers->userId = $this->data['users']->id;
        $assignmentsAnswers->userNotes = Input::get('userNotes');
        $assignmentsAnswers->userTime = time();
        if (!Input::hasFile('fileName')) {
            return $this->panelInit->apiOutput(false, $this->panelInit->language['applyAssAnswer'], $this->panelInit->language['assNoFilesUploaded']);
        } elseif (Input::hasFile('fileName')) {
            $fileInstance = Input::file('fileName');
            $newFileName = "assignments_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
            $fileInstance->move('uploads/assignmentsAnswers/', $newFileName);

            $assignmentsAnswers->fileName = $newFileName;
            $assignmentsAnswers->save();
        }
        $assignmentsAnswers->save();

        return $this->panelInit->apiOutput(true, $this->panelInit->language['applyAssAnswer'], $this->panelInit->language['assUploadedSucc']);
    }

    function listAnswers($id) {
        if ($this->data['users']->role == "student" || $this->data['users']->role == "parent")
            exit;

        $assignmentsAnswers = \DB::table('assignments_answers')
                ->leftJoin('users', 'users.id', '=', 'assignments_answers.userId')
                ->leftJoin('classes', 'classes.id', '=', 'users.studentClass')
                ->select('assignments_answers.id as id', 'assignments_answers.userId as userId', 'assignments_answers.userNotes as userNotes', 'assignments_answers.userTime as userTime', 'assignments_answers.fileName as AssignFile', 'users.fullName as fullName', 'classes.className as className')
                ->where('assignmentId', $id)
                ->get();

        return $assignmentsAnswers;
    }

    public function downloadAnswer($id) {
        $toReturn = assignments_answers::where('id', $id)->first();
        $user = User::where('id', $toReturn->userId)->first();
        if (file_exists('uploads/assignmentsAnswers/' . $toReturn->fileName)) {
            $fileName = preg_replace('/[^a-zA-Z0-9-_\.]/', '-', $user->fullName) . "." . pathinfo($toReturn->fileName, PATHINFO_EXTENSION);
            header("Content-Type: application/force-download");
            header("Content-Disposition: attachment; filename=" . $fileName);
            echo file_get_contents('uploads/assignmentsAnswers/' . $toReturn->fileName);
        }
        exit;
    }

//    public function export() {
//        if ($this->data['users']->role != "admin")
//            exit;
//        $subjectArray = array();
//        $subjects = subject::get();
//        foreach ($subjects as $subject) {
//            $subjectArray[$subject->id] = $subject->subjectTitle;
//        }
//        $data = array(1 => array('Full Name', 'User Name', 'E-mail', 'Gender', 'Address', 'Phone No', 'Mobile No', 'birthday', 'password', 'Subject'));
//        $student = User::where('role', 'teacher')->get();
//        foreach ($student as $value) {
//            $birthday = "";
//            if ($value->birthday != 0) {
//                $birthday = $this->panelInit->unixToDate($value->birthday);
//            }
//            $data[] = array($value->fullName, $value->username, $value->email, $value->gender, $value->address, $value->phoneNo, $value->mobileNo, $birthday, "", isset($subjectArray[$value->subject]) ? $subjectArray[$value->subject] : "");
//        }
//
//        $xls = new Excel_XML('UTF-8', false, 'Teachers Sheet');
//        $xls->addArray($data);
//        $xls->generateXML('Teachers-Sheet');
//        exit;
//    }
}
