<?php

class StudyMaterialController extends \BaseController {

    var $data = array();
    var $panelInit;
    var $layout = 'dashboard';

    public function __construct() {
        $this->panelInit = new \DashboardInit();
        $this->data['panelInit'] = $this->panelInit;
        $this->data['users'] = \Auth::user();

        if (!$this->data['users']->hasThePerm('studyMaterial')) {
            exit;
        }
    }

    public function listAll() {
          DB::table('mob_notifications')
                ->where('type_notify', 'lesson')
                ->where('read_notify', '0')
                ->where('notifToIds', $this->data['users']->id)
                ->update(['read_notify' => '1']);
        $toReturn = array();
        if ($this->data['users']->role == "teacher") {
            $toReturn['classes'] = classes::where('classAcademicYear', $this->panelInit->selectAcYear)->where('classTeacher', 'LIKE', '%"' . $this->data['users']->id . '"%')->get()->toArray();
            $subjects = subject::where('id', $this->data['users']->subject)->get()->toArray();
            $subjectArray = array();
            while (list(, $subject) = each($subjects)) {
                $subjectArray[$subject['id']] = $subject['subjectTitle'];
            }
        } else {
            $toReturn['classes'] = classes::where('classAcademicYear', $this->panelInit->selectAcYear)->get()->toArray();
            $subjects = subject::get()->toArray();
            $subjectArray = array();
            while (list(, $subject) = each($subjects)) {
                $subjectArray[$subject['id']] = $subject['subjectTitle'];
            }
        }
        $classesArray = array();
        while (list(, $class) = each($toReturn['classes'])) {
            $classesArray[$class['id']] = $class['className'];
        }
        $sections = sections::get()->toArray();
        $sectionArray = array();
        while (list(, $section) = each($sections)) {
            $sectionArray[$section['id']] = $section['sectionName'];
        }

        $toReturn['materials'] = array();
        $studyMaterial = new study_material();

        if ($this->data['users']->role == "student") {
            $studyMaterial = $studyMaterial->where('class_id', 'LIKE', '%"' . $this->data['users']->studentClass . '"%');
            if ($this->panelInit->settingsArray['enableSections'] == true) {
                $studyMaterial = $studyMaterial->where('sectionId', 'LIKE', '%"' . $this->data['users']->studentSection . '"%');
            }
        }

        if ($this->data['users']->role == "teacher") {
            //$studyMaterial = $studyMaterial->where('teacher_id', $this->data['users']->id);
            $studyMaterial = $studyMaterial->where('subject_id', $this->data['users']->subject)
                    ->where('teacher_id', $this->data['users']->id);
        }

        $studyMaterial = $studyMaterial->get();

        foreach ($studyMaterial as $key => $material) {
            $classId = json_decode($material->class_id);
            $sectionId = json_decode($material->sectionId);
            if ($this->data['users']->role == "student" AND ! in_array($this->data['users']->studentClass, $classId) And ! in_array($this->data['users']->studentSection, $sectionId)) {
                continue;
            }
            $toReturn['materials'][$key]['id'] = $material->id;
            $toReturn['materials'][$key]['subjectId'] = $material->subject_id;
            $toReturn['materials'][$key]['subject'] = $subjectArray[$material->subject_id];
            // $toReturn['materials'][$key]['class'] = $classesArray[$material->class_id];
            //       $toReturn['materials'][$key]['section'] = $sectionArray[$material->sectionId];
            $toReturn['materials'][$key]['teacher_id'] = $material->teacher_id;
            $toReturn['materials'][$key]['material_title'] = $material->material_title;
            $toReturn['materials'][$key]['material_description'] = $material->material_description;
            $toReturn['materials'][$key]['material_description'] = htmlspecialchars_decode($toReturn['materials'][$key]['material_description']);
            $toReturn['materials'][$key]['material_file'] = $material->material_file;
            $toReturn['materials'][$key]['materialdate'] = $this->panelInit->unixToDate($material->materialdate);
            $toReturn['materials'][$key]['material_link'] = $material->material_link;
            $toReturn['materials'][$key]['materialAuthor'] = $material->materialAuthor;
            $toReturn['materials'][$key]['classes'] = "";
            $toReturn['materials'][$key]['sections'] = "";
            if (is_array($classId)) {
                while (list(, $value) = each($classId)) {
                    if (isset($classesArray[$value])) {
                        $toReturn['materials'][$key]['classes'] .= $classesArray[$value] . ", ";
                    }
                }
            }
            if (is_array($sectionId)) {
                while (list(, $value) = each($sectionId)) {
                    if (isset($sectionArray[$value])) {
                        $toReturn['materials'][$key]['sections'] .= $sectionArray[$value] . ", ";
                    }
                }
            }
        }

        $toReturn['userRole'] = $this->data['users']->role;
        $toReturn['id'] = $this->data['users']->id;
        return $toReturn;
        exit;
    }

    public function delete($id) {
        if ($this->data['users']->role == "student" || $this->data['users']->role == "parent")
            exit;
        if ($postDelete = study_material::where('id', $id)->first()) {
            @unlink('uploads/studyMaterial/' . $postDelete->material_file);
            $postDelete->delete();
            return $this->panelInit->apiOutput(true, $this->panelInit->language['delMaterial'], $this->panelInit->language['materialDel']);
        } else {
            return $this->panelInit->apiOutput(false, $this->panelInit->language['delMaterial'], $this->panelInit->language['materialNotExist']);
        }
    }

    public function create() {
        if ($this->data['users']->role == "student" || $this->data['users']->role == "parent")
            exit;
        $studyMaterial = new study_material();
        $studyMaterial->class_id = json_encode(Input::get('classId'));
        //   if ($this->panelInit->settingsArray['enableSections'] == true) {
        $studyMaterial->sectionId = json_encode(Input::get('sectionId'));
        //  }
        $studyMaterial->subject_id = Input::get('subject_id');
        $studyMaterial->material_title = Input::get('material_title');
        $studyMaterial->material_link = Input::get('material_link');
        $studyMaterial->materialAuthor = $this->data['users']->fullName;
        // if (Input::get('materialdate')) {
        $studyMaterial->materialdate = $this->panelInit->dateToUnix(Input::get('materialdate'));
        // }
        // $studyMaterial->material_description = Input::get('material_description');
        $studyMaterial->material_description = htmlspecialchars(Input::get('material_description'), ENT_QUOTES);
        $studyMaterial->teacher_id = $this->data['users']->id;
        $studyMaterial->save();
        if (Input::hasFile('material_file')) {
            $fileInstance = Input::file('material_file');
            $newFileName = "material_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
            $fileInstance->move('uploads/studyMaterial/', $newFileName);

            $studyMaterial->material_file = $newFileName;
            $studyMaterial->save();
        }
        $classes = Input::get('classId');
        $sections = Input::get('sectionId');
        while (list(, $value) = each($classes)) {
            $classesList[] = $value;
        }
        while (list(, $value) = each($sections)) {
            $sectionsList[] = $value;
        }
        $students = User::where('role', 'student')->whereIn('studentClass', $classesList)->whereIn('studentSection', $sectionsList)->get()->toArray();
        if (!empty($students)) {
            foreach ($students as $student) {
                $this->panelInit->mobNotifyUser('class', $student['id'], $this->panelInit->language['newMatrialAdded'] . " " . Input::get('material_title'), 'lesson');
            }
        }
        $studyMaterial->material_description = htmlspecialchars_decode($studyMaterial->material_description);
        $studyMaterial->materialdate = $this->panelInit->unixToDate($studyMaterial->materialdate);
        return $this->panelInit->apiOutput(true, $this->panelInit->language['addMaterial'], $this->panelInit->language['materialAdded'], $studyMaterial->toArray());
    }

    function fetch($id) {
        $studyMaterial = study_material::where('id', $id)->first()->toArray();
        $studyMaterial['material_description'] = htmlspecialchars_decode($studyMaterial['material_description'], ENT_QUOTES);
        $DashboardController = new DashboardController();
        $studyMaterial['sections'] = $DashboardController->sectionsList(json_decode($studyMaterial['class_id'], true));
        $studyMaterial['subject'] = $DashboardController->subjectList(json_decode($studyMaterial['class_id'], true));
        return $studyMaterial;
    }

    public function download($id) {
        $toReturn = study_material::where('id', $id)->first();
        if (file_exists('uploads/studyMaterial/' . $toReturn->material_file)) {
            $fileName = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $toReturn->material_title) . "." . pathinfo($toReturn->material_file, PATHINFO_EXTENSION);
            header("Content-Type: application/force-download");
            header("Content-Disposition: attachment; filename=" . $fileName);
            echo file_get_contents('uploads/studyMaterial/' . $toReturn->material_file);
        }
        exit;
    }

    function edit($id) {
        if ($this->data['users']->role == "student" || $this->data['users']->role == "parent")
            exit;
        $studyMaterial = study_material::find($id);
        $studyMaterial->class_id = json_encode(Input::get('classId'));
        //  if ($this->panelInit->settingsArray['enableSections'] == true) {
        $studyMaterial->sectionId = json_encode(Input::get('sectionId'));
        //  }
        $studyMaterial->subject_id = Input::get('subject_id');
        $studyMaterial->material_title = Input::get('material_title');
        $studyMaterial->material_link = Input::get('material_link');
        $studyMaterial->materialAuthor = $this->data['users']->fullName;
        // if (Input::get('materialdate')) {
        $studyMaterial->materialdate = $this->panelInit->dateToUnix(Input::get('materialdate'));
        // }
        //  $studyMaterial->material_description = Input::get('material_description');
        $studyMaterial->material_description = htmlspecialchars(Input::get('material_description'), ENT_QUOTES);
        if (Input::hasFile('material_file')) {
            @unlink("uploads/studyMaterial/" . $studyMaterial->material_file);
            $fileInstance = Input::file('material_file');
            $newFileName = "material_" . uniqid() . "." . $fileInstance->getClientOriginalExtension();
            $fileInstance->move('uploads/studyMaterial/', $newFileName);

            $studyMaterial->material_file = $newFileName;
        }
        $studyMaterial->save();
        $studyMaterial->material_description = htmlspecialchars_decode($studyMaterial->material_description);
        $studyMaterial->materialdate = $this->panelInit->unixToDate($studyMaterial->materialdate);
        return $this->panelInit->apiOutput(true, $this->panelInit->language['editMaterial'], $this->panelInit->language['materialEdited'], $studyMaterial->toArray());
    }

}
