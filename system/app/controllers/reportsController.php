<?php

class reportsController extends \BaseController {

    var $data = array();
    var $panelInit;
    var $layout = 'dashboard';

    public function __construct() {
        $this->panelInit = new \DashboardInit();
        $this->data['panelInit'] = $this->panelInit;
        $this->data['breadcrumb']['Settings'] = \URL::to('/dashboard/languages');
        $this->data['users'] = \Auth::user();

        if (!$this->data['users']->hasThePerm('Reports')) {
            exit;
        }
    }

    public function report() {
        if ($this->data['users']->role != "admin")
            exit;
        if (Input::get('stats') == 'usersStats') {
            return $this->usersStats();
        }
        if (Input::get('stats') == 'stdAttendance') {
            return $this->stdAttendance(Input::get('data'));
        }
        if (Input::get('stats') == 'stfAttendance') {
            return $this->stfAttendance(Input::get('data'));
        }
        if (Input::get('stats') == 'stdVacation') {
            return $this->stdVacation(Input::get('data'));
        }
        if (Input::get('stats') == 'stfVacation') {
            return $this->stfVacation(Input::get('data'));
        }
        if (Input::get('stats') == 'payments') {
            return $this->reports(Input::get('data'));
        }
        if (Input::get('stats') == 'marksheetGenerationPrepare') {
            return $this->marksheetGenerationPrepare();
        }
        if (Input::get('stats') == 'achievmentPrepare') {
            return $this->achievmentPrepare();
        }
        if (Input::get('stats') == 'achievementGet') {
            return $this->achievementGet(Input::get('data'));
        }
        if (Input::get('stats') == 'averageGet') {
            return $this->test_Average(Input::get('data'));
        }
    }

    public function usersStats() {
        $toReturn = array();
        $toReturn['admins'] = array();
        $toReturn['admins']['activated'] = User::where('role', 'admin')->where('activated', '1')->count();
        $toReturn['admins']['inactivated'] = User::where('role', 'admin')->where('activated', '0')->count();
        $toReturn['admins']['total'] = $toReturn['admins']['activated'] + $toReturn['admins']['inactivated'];

        $toReturn['teachers'] = array();
        $toReturn['teachers']['activated'] = User::where('role', 'teacher')->where('activated', '1')->count();
        $toReturn['teachers']['inactivated'] = User::where('role', 'teacher')->where('activated', '0')->count();
        $toReturn['teachers']['total'] = $toReturn['teachers']['activated'] + $toReturn['teachers']['inactivated'];

        $toReturn['students'] = array();
        $toReturn['students']['activated'] = User::where('role', 'student')->where('activated', '1')->count();
        $toReturn['students']['inactivated'] = User::where('role', 'student')->where('activated', '0')->count();
        $toReturn['students']['total'] = $toReturn['students']['activated'] + $toReturn['students']['inactivated'];

        $toReturn['parents'] = array();
        $toReturn['parents']['activated'] = User::where('role', 'parent')->where('activated', '1')->count();
        $toReturn['parents']['inactivated'] = User::where('role', 'parent')->where('activated', '0')->count();
        $toReturn['parents']['total'] = $toReturn['parents']['activated'] + $toReturn['parents']['inactivated'];

        return $toReturn;
    }

    public function preAttendaceStats() {
        $toReturn = array();
        $classes = classes::where('classAcademicYear', $this->panelInit->selectAcYear)->get();
        $toReturn['classes'] = array();
        $subjList = array();
        foreach ($classes as $class) {
            $class['classSubjects'] = json_decode($class['classSubjects'], true);
            if (is_array($class['classSubjects'])) {
                foreach ($class['classSubjects'] as $subject) {
                    $subjList[] = $subject;
                }
            }
            $toReturn['classes'][$class->id] = $class->className;
        }

        $subjList = array_unique($subjList);
        if ($this->data['panelInit']->settingsArray['attendanceModel'] == "subject") {
            $toReturn['subjects'] = array();
            if (count($subjList) > 0) {
                $subjects = subject::whereIN('id', $subjList)->get();
                foreach ($subjects as $subject) {
                    $toReturn['subjects'][$subject->id] = $subject->subjectTitle;
                }
            }
        }

        $toReturn['role'] = $this->data['users']->role;
        $toReturn['attendanceModel'] = $this->data['panelInit']->settingsArray['attendanceModel'];

        return $toReturn;
    }

    public function stdAttendance($data) {
        $sql = "select * from attendance where ";
        $sqlArray = array();
        $toReturn = array();

        $students = array();
        $studentArray = User::where('role', 'student');
        if (isset($data['classId']) AND $data['classId'] != "") {
            $studentArray = $studentArray->where('studentClass', $data['classId']);
        }
        if (isset($data['sectionId']) AND $data['sectionId'] != "") {
            $studentArray = $studentArray->where('studentSection', $data['sectionId']);
        }
        $studentArray = $studentArray->get();
        foreach ($studentArray as $stOne) {
            $students[$stOne->id] = array('name' => $stOne->fullName, 'studentRollId' => $stOne->studentRollId, 'attendance' => '');
        }

        $subjectsArray = subject::get();
        $subjects = array();
        foreach ($subjectsArray as $subject) {
            $subjects[$subject->id] = $subject->subjectTitle;
        }

        if (isset($data['classId']) AND $data['classId'] != "") {
            $sqlArray[] = "classId='" . $data['classId'] . "'";
        }
        if ($this->data['panelInit']->settingsArray['attendanceModel'] == "subject" AND isset($data['subjectId']) AND $data['subjectId'] != "") {
            $sqlArray[] = "subjectId='" . $data['subjectId'] . "'";
        }
        if (isset($data['status']) AND $data['status'] != "All") {
            $sqlArray[] = "status='" . $data['status'] . "'";
        }

        if (isset($data['attendanceDayFrom']) AND $data['attendanceDayFrom'] != "" AND isset($data['attendanceDayTo']) AND $data['attendanceDayTo'] != "") {
            $days = $this->panelInit->rangeDates($data['attendanceDayFrom'], $data['attendanceDayTo']);
            $sqlArray[] = "date > (" . $days['start'] . ") AND date < (" . $days['end'] . ") ";
        }

        $sql = $sql . implode(" AND ", $sqlArray);
        $attendanceArray = DB::select(DB::raw($sql));

        foreach ($attendanceArray as $stAttendance) {
            if (isset($students[$stAttendance->studentId])) {
                $toReturn[$stAttendance->id] = $stAttendance;
                $toReturn[$stAttendance->id]->studentName = $students[$stAttendance->studentId]['name'];
                if ($stAttendance->subjectId != "") {
                    $toReturn[$stAttendance->id]->studentSubject = $subjects[$stAttendance->subjectId];
                }
                $toReturn[$stAttendance->id]->studentRollId = $students[$stAttendance->studentId]['studentRollId'];
            }
        }

        if (isset($data['exportType']) AND $data['exportType'] == "excel") {
            $data = array(1 => array('Date', 'Roll Id', 'Full Name', 'Subject', 'Status'));

            foreach ($toReturn as $value) {
                if ($value->status == 0) {
                    $value->status = $this->panelInit->language['Absent'];
                } elseif ($value->status == 1) {
                    $value->status = $this->panelInit->language['Present'];
                } elseif ($value->status == 2) {
                    $value->status = $this->panelInit->language['Late'];
                } elseif ($value->status == 3) {
                    $value->status = $this->panelInit->language['LateExecuse'];
                }
                $data[] = array($this->panelInit->unixToDate($value->date), (isset($value->studentRollId) ? $value->studentRollId : ""), (isset($value->studentName) ? $value->studentName : ""), (isset($value->studentSubject) ? $value->studentSubject : ""), $value->status);
            }

            $xls = new Excel_XML('UTF-8', false, 'Students Atendance Report');
            $xls->addArray($data);
            $xls->generateXML('Students Atendance Report');
            exit;
        }

        if (isset($data['exportType']) AND $data['exportType'] == "pdf") {
            $header = array('Date', 'Roll Id', 'Full Name', 'Subject', 'Status');
            $data = array();
            foreach ($toReturn as $value) {
                if ($value->status == 0) {
                    $value->status = $this->panelInit->language['Absent'];
                } elseif ($value->status == 1) {
                    $value->status = $this->panelInit->language['Present'];
                } elseif ($value->status == 2) {
                    $value->status = $this->panelInit->language['Late'];
                } elseif ($value->status == 3) {
                    $value->status = $this->panelInit->language['LateExecuse'];
                }
                $data[] = array($this->panelInit->unixToDate($value->date), (isset($value->studentRollId) ? $value->studentRollId : ""), (isset($value->studentName) ? $value->studentName : ""), (isset($value->studentSubject) ? $value->studentSubject : ""), $value->status);
            }

            $pdf = new FPDF();
            $pdf->SetFont('Arial', '', 10);
            $pdf->AddPage();
            // Header
            foreach ($header as $col) {
                $pdf->Cell(40, 7, $col, 1);
            }
            $pdf->Ln();
            // Data
            foreach ($data as $row) {
                foreach ($row as $col) {
                    $pdf->Cell(40, 6, $col, 1);
                }
                $pdf->Ln();
            }
            $pdf->Output();
            exit;
        }

        return $toReturn;
    }

    public function stfAttendance($data) {
        $sql = "select * from attendance where ";
        $sqlArray = array();
        $toReturn = array();

        $teachers = array();
        $studentArray = User::where('role', 'teacher')->get();
        foreach ($studentArray as $stOne) {
            $teachers[$stOne->id] = array('name' => $stOne->fullName, 'attendance' => '');
        }

        if (isset($data['status']) AND $data['status'] != "All") {
            $sqlArray[] = "status='" . $data['status'] . "'";
        }

        if (isset($data['attendanceDayFrom']) AND $data['attendanceDayFrom'] != "" AND isset($data['attendanceDayTo']) AND $data['attendanceDayTo'] != "") {
            $days = $this->panelInit->rangeDates($data['attendanceDayFrom'], $data['attendanceDayTo']);
            $sqlArray[] = "date > (" . $days['start'] . ") AND date < (" . $days['end'] . ") ";
        }

        $sqlArray[] = "classId = '0'";

        $sql = $sql . implode(" AND ", $sqlArray);
        $attendanceArray = DB::select(DB::raw($sql));

        foreach ($attendanceArray as $stAttendance) {
            $toReturn[$stAttendance->id] = $stAttendance;
            if (isset($teachers[$stAttendance->studentId])) {
                $toReturn[$stAttendance->id]->studentName = $teachers[$stAttendance->studentId]['name'];
            }
        }

        if (isset($data['exportType']) AND $data['exportType'] == "excel") {
            $data = array(1 => array('Date', 'Full Name', 'Status'));
            foreach ($toReturn as $value) {
                if ($value->status == 0) {
                    $value->status = $this->panelInit->language['Absent'];
                } elseif ($value->status == 1) {
                    $value->status = $this->panelInit->language['Present'];
                } elseif ($value->status == 2) {
                    $value->status = $this->panelInit->language['Late'];
                } elseif ($value->status == 3) {
                    $value->status = $this->panelInit->language['LateExecuse'];
                }
                $data[] = array($this->panelInit->unixToDate($value->date), $value->studentName, $value->status);
            }

            $xls = new Excel_XML('UTF-8', false, 'Staff Atendance Report');
            $xls->addArray($data);
            $xls->generateXML('Staff Atendance Report');
            exit;
        }

        if (isset($data['exportType']) AND $data['exportType'] == "pdf") {
            $header = array('Date', 'Full Name', 'Status');
            $data = array();
            foreach ($toReturn as $value) {
                if ($value->status == 0) {
                    $value->status = $this->panelInit->language['Absent'];
                } elseif ($value->status == 1) {
                    $value->status = $this->panelInit->language['Present'];
                } elseif ($value->status == 2) {
                    $value->status = $this->panelInit->language['Late'];
                } elseif ($value->status == 3) {
                    $value->status = $this->panelInit->language['LateExecuse'];
                }
                $data[] = array($this->panelInit->unixToDate($value->date), $value->studentName, $value->status);
            }

            $pdf = new FPDF();
            $pdf->SetFont('Arial', '', 10);
            $pdf->AddPage();
            // Header
            foreach ($header as $col) {
                $pdf->Cell(40, 7, $col, 1);
            }
            $pdf->Ln();
            // Data
            foreach ($data as $row) {
                foreach ($row as $col) {
                    $pdf->Cell(40, 6, $col, 1);
                }
                $pdf->Ln();
            }
            $pdf->Output();
            exit;
        }

        return $toReturn;
    }

    public function stdVacation($data) {
        $datesList = $this->panelInit->rangeDates($data['fromDate'], $data['toDate']);

        if (count($datesList) > 0) {
            $vacationList = \DB::table('vacation')
                    ->leftJoin('users', 'users.id', '=', 'vacation.userid')
                    ->select('vacation.id as id', 'vacation.userid as userid', 'vacation.vacDate as vacDate', 'vacation.acceptedVacation as acceptedVacation', 'users.fullName as fullName')
                    ->where('vacation.acYear', $this->panelInit->selectAcYear)
                    ->where('vacation.role', 'student')
                    ->where('vacation.vacDate', '>', $datesList['start'])
                    ->where('vacation.vacDate', '<', $datesList['end'])
                    ->get();

            return $vacationList;
        }

        return array();
    }

    public function stfVacation($data) {
        $datesList = $this->panelInit->rangeDates($data['fromDate'], $data['toDate']);

        if (count($datesList) > 0) {
            $vacationList = \DB::table('vacation')
                    ->leftJoin('users', 'users.id', '=', 'vacation.userid')
                    ->select('vacation.id as id', 'vacation.userid as userid', 'vacation.vacDate as vacDate', 'vacation.acceptedVacation as acceptedVacation', 'users.fullName as fullName')
                    ->where('vacation.acYear', $this->panelInit->selectAcYear)
                    ->where('vacation.role', 'teacher')
                    ->where('vacation.vacDate', '>', $datesList['start'])
                    ->where('vacation.vacDate', '<', $datesList['end'])
                    ->get();

            return $vacationList;
        }

        return array();
    }

    public function reports($data) {
        $datesList = $this->panelInit->rangeDates($data['fromDate'], $data['toDate']);

        $payments = \DB::table('payments')
                ->leftJoin('users', 'users.id', '=', 'payments.paymentStudent')
                ->where('payments.paymentDate', '>', $datesList['start'])
                ->where('payments.paymentDate', '<', $datesList['end'])
                ->select('payments.id as id', 'payments.paymentTitle as paymentTitle', 'payments.paymentDescription as paymentDescription', 'payments.paymentAmount as paymentAmount', 'payments.paymentStatus as paymentStatus', 'payments.paymentDate as paymentDate', 'payments.paymentStudent as studentId', 'users.fullName as fullName');

        if ($data['status'] != "All") {
            $payments = $payments->where('paymentStatus', $data['status']);
        }
        $payments = $payments->where('paymentDate', '>', $datesList['start'])->where('paymentDate', '<', $datesList['end'])->orderBy('id', 'DESC')->get();

        return $payments;
    }

    public function marksheetGenerationPrepare() {
        $toReturn = array();
        $toReturn['classes'] = classes::where('classAcademicYear', $this->panelInit->selectAcYear)->get()->toArray();
        $toReturn['exams'] = exams_list::where('examAcYear', $this->panelInit->selectAcYear)->get()->toArray();
        return $toReturn;
    }

    public function achievmentPrepare() {
        $toReturn = array();
        $toReturn['classes'] = classes::where('classAcademicYear', $this->panelInit->selectAcYear)->get()->toArray();
        if ($this->data['users']->role == "teacher") {
            $toReturn['subjects'] = subject::where('id', $this->data['users']->subject)->get()->toArray();
        } else {
            $toReturn['subjects'] = subject::get()->toArray();
        }
        //  $toReturn['exams'] = online_exams::where('exAcYear', $this->panelInit->selectAcYear)->get()->toArray();
        return $toReturn;
    }

    public function onlineExamfilter() {
        $toReturn = array();
        $classes = Input::get('classes');
        $sections = Input::get('sections');
        $subjects = Input::get('subjects');
        if ($classes != "" && $sections != "" && $subjects != "") {
            $toReturn['exams'] = online_exams::where('examClass', 'LIKE', '%"' . $classes . '"%')
                            ->where('sectionId', 'LIKE', '%"' . $sections . '"%')
                            ->where('examSubject', $subjects)
                            ->where('examTeacher', '!=', '0')
                            ->where('exAcYear', $this->panelInit->selectAcYear)->get()->toArray();
        }
        return $toReturn;
    }

    public function onlineExamAvgfilter() {
        $toReturn = array();
        $classes = Input::get('classes');
        $subjects = Input::get('subjects');
        if ($classes != "" && $subjects != "") {
            $toReturn['exams'] = online_exams::where('examClass', 'LIKE', '%"' . $classes . '"%')
                            ->where('examSubject', $subjects)
                            ->where('examTeacher', '0')
                            ->where('exAcYear', $this->panelInit->selectAcYear)->get()->toArray();
        }
        return $toReturn;
    }

    public function achievementGet($data) {
        $toReturn = array();
        $class_id = $data['classId'];
        $section_id = $data['studentSection'];
        $subject_id = $data['subjectId'];
        $exam_id = $data['examId'];
        $arr_final = array();
        $arr_test = array();
        $student = array(); 
        $m = 1;
        $onlineExams = online_exams::where('id', $exam_id)->first();
        $user = User::where('studentClass', $class_id)->where('studentSection', $section_id);
        foreach ($user as $value) {
            $student[] = $value['id'];
        }
        $onlineExamsexamQuestion = json_decode($onlineExams['examQuestion'], true);
        $examQuestion = json_decode($onlineExams['examQuestion'], true);
        $exams = online_exams_grades::
                        where('examId', $exam_id)
                        ->leftJoin('users', 'users.id', '=', 'online_exams_grades.studentId')
                        ->select('online_exams_grades.examId', 'online_exams_grades.examQuestionsAnswers', 'online_exams_grades.examGrade', 'users.fullname')
                        ->get()->toArray();
        foreach ($exams as $exam) {
            $examQuestionsAnswers = json_decode($exam['examQuestionsAnswers'], true);
            $toReturn['student'][1] = array('fullname' => $exam['fullname']);
            $sum = 0;
            for ($i = 0; $i < count($examQuestionsAnswers); $i++) {
                if (isset($examQuestionsAnswers[$i]['type']) AND $examQuestionsAnswers[$i]['type'] == 'radio') {
                    if (isset($onlineExamsexamQuestion[$i]['Tans'])) {
                        if ($examQuestionsAnswers[$i]['answer'] == $onlineExamsexamQuestion[$i]['Tans']) {
                            if (isset($onlineExamsexamQuestion[$i]['questionMark'])) {
                                $toReturn['student'][2][$i] = $onlineExamsexamQuestion[$i]['questionMark'];
                            } else {
                                $toReturn['student'][2][$i] = 1;
                            }
                        } else {
                            $toReturn['student'][2][$i] = 0;
                        }
                    }
                }
                if (isset($examQuestionsAnswers[$i]['type']) AND $examQuestionsAnswers[$i]['type'] == 'check') {
                    $str_checkType = '';
                    $str_check_exam = '';
                    if (isset($onlineExamsexamQuestion[$i]['Tans1'])) {
                        if (isset($examQuestionsAnswers[$i]['answer1']) AND $examQuestionsAnswers[$i]['answer1'] == 'true') {
                            $str_checkType .= ',' . $examQuestionsAnswers[$i]['ans1'];
                            $str_check_exam .= ',' . $onlineExamsexamQuestion[$i]['ans1'];
                        }
                    }
                    if (isset($onlineExamsexamQuestion[$i]['Tans2'])) {
                        if (isset($examQuestionsAnswers[$i]['answer2']) AND $examQuestionsAnswers[$i]['answer2'] == 'true') {
                            $str_checkType .=',' . $examQuestionsAnswers[$i]['ans2'];
                            $str_check_exam .= ',' . $onlineExamsexamQuestion[$i]['ans2'];
                        }
                    }if (isset($onlineExamsexamQuestion[$i]['Tans3'])) {
                        if (isset($examQuestionsAnswers[$i]['answer3']) AND $examQuestionsAnswers[$i]['answer3'] == 'true') {
                            $str_checkType .= ',' . $examQuestionsAnswers[$i]['ans3'];
                            $str_check_exam .= ',' . $onlineExamsexamQuestion[$i]['ans3'];
                        }
                    }if (isset($onlineExamsexamQuestion[$i]['Tans4'])) {
                        if (isset($examQuestionsAnswers[$i]['answer4']) AND $examQuestionsAnswers[$i]['answer4'] == 'true') {
                            $str_checkType .=',' . $examQuestionsAnswers[$i]['ans4'];
                            $str_check_exam .= ',' . $onlineExamsexamQuestion[$i]['ans4'];
                        }
                    }
                    if ($str_check_exam == $str_checkType) {
                        if (isset($onlineExamsexamQuestion[$i]['questionMark'])) {
                            $toReturn['student'][2][$i] = $onlineExamsexamQuestion[$i]['questionMark'];
                        } else {
                            $toReturn['student'][2][$i] = 1;
                        }
                    } else {
                        $toReturn['student'][2][$i] = 0;
                    }
                    //  $toReturn['student']['questions']['Question' . $i] = array('answer' => $str_checkType);
                }
                if (isset($examQuestionsAnswers[$i]['type']) AND $examQuestionsAnswers[$i]['type'] == 'text') {
                    if ($onlineExamsexamQuestion[$i]['ans1'] == $examQuestionsAnswers[$i]['answer']) {
                        if (isset($onlineExamsexamQuestion[$i]['questionMark'])) {
                            $toReturn['student'][2][$i] = $onlineExamsexamQuestion[$i]['questionMark'];
                        } else {
                            $toReturn['student'][2][$i] = 1;
                        }
                    } else {
                        $toReturn['student'][2][$i] = 0;
                    }
                }
                $sum+= $toReturn['student'][2][$i];
            }
            $toReturn['student'][3] = array('sum' => $sum);
            array_push($arr_final, $toReturn);
        }
        $to = array();
        $total = 0;
      
        for ($k = 0; $k < count($onlineExamsexamQuestion); $k++) {
            $question = 'Question' . $m;
//            for ($i = 0; $i < count($arr_final); $i++) {
            if (isset($onlineExamsexamQuestion[$k]['questionMark'])) {
                $toReturn['final'][$question]['mark'] = $onlineExamsexamQuestion[$k]['questionMark'];
            } else {
                $toReturn['final'][$question]['mark'] = 1;
            }

            $total+=$toReturn['final'][$question]['mark'];
            //  array_push($arr_test, $to);
//            }
            $m++;
        }
        $toReturn['final']['total'] = $user;
        $toReturn['student'] = $arr_final;
        return $toReturn;
    }

    public function averageGet($data) {
        $toReturn = array();
        $class_id = $data['classId'];
        $subject_id = $data['subjectId'];
        $exam_id = $data['examId'];
    }

    private function getSumGradeStudent($exam_id) {
        $exams = online_exams_grades::
                        where('examId', $exam_id)
                        ->leftJoin('users', 'users.id', '=', 'online_exams_grades.studentId')
                        ->select('online_exams_grades.examId', 'online_exams_grades.examQuestionsAnswers', 'online_exams_grades.examGrade', 'users.fullname')
                        ->get()->toArray();
    }

    public function test_Average($data) {
        $toReturn = array();
        $class_id = $data['classId'];
        $subject_id = $data['subjectId'];
        $exam_id = $data['examId'];
        $arr_final = array();
        $arr_test = array();
        $onlineExams = online_exams::where('id', $exam_id)->first();
        //sections array
        $sections = sections::where('classId', $class_id)->get()->toArray();
        $examsection = json_decode($onlineExams['sectionId'], true);
        $sectionsArray = array();
        while (list(, $section) = each($sections)) {
            $sectionsArray[$section['id']] = $section['sectionName'];
            $onlineExams = online_exams::where('id', $exam_id)->where('sectionId', 'LIKE', '%"' . $section['id'] . '"%')->first();
            $onlineExamsexamQuestion = json_decode($onlineExams['examQuestion'], true);

            for ($i = 0; $i < count($onlineExamsexamQuestion); $i++) {
                $toReturn['questions'][$i] = 'Question' . $i;
                $mark = 1;
                if (isset($onlineExamsexamQuestion[$i]['questionMark'])) {
                    $mark = $onlineExamsexamQuestion[$i]['questionMark'];
                }
                $users = User::where('studentSection', $section['id'])->where('role', 'student')->get()->toArray();
                $sum = 0;
                $average = 0;
                foreach ($users as $user) {
                    $onlineExamsgrades = online_exams_grades::where('examId', $exam_id)->where('studentId', $user['id'])->first();
                    $examgradesQuestion = json_decode($onlineExamsgrades['examQuestionsAnswers'], true);
                    /////
                    if (isset($examgradesQuestion[$i]['type']) AND $examgradesQuestion[$i]['type'] == 'radio') {
                        if (isset($onlineExamsexamQuestion[$i]['Tans'])) {
                            if ($examgradesQuestion[$i]['answer'] == $onlineExamsexamQuestion[$i]['Tans']) {
                                if (isset($onlineExamsexamQuestion[$i]['questionMark'])) {
                                    $sum+= $onlineExamsexamQuestion[$i]['questionMark'];
                                } else {
                                    $sum+= 1;
                                }
                            } else {
                                $sum+= 0;
                            }
                        }
                    }
                    if (isset($examgradesQuestion[$i]['type']) AND $examgradesQuestion[$i]['type'] == 'check') {
                        $str_checkType = '';
                        $str_check_exam = '';
                        if (isset($onlineExamsexamQuestion[$i]['Tans1'])) {
                            if (isset($examgradesQuestion[$i]['answer1']) AND $examgradesQuestion[$i]['answer1'] == 'true') {
                                $str_checkType .= ',' . $examgradesQuestion[$i]['ans1'];
                                $str_check_exam .= ',' . $onlineExamsexamQuestion[$i]['ans1'];
                            }
                        }
                        if (isset($onlineExamsexamQuestion[$i]['Tans2'])) {
                            if (isset($examgradesQuestion[$i]['answer2']) AND $examgradesQuestion[$i]['answer2'] == 'true') {
                                $str_checkType .=',' . $examgradesQuestion[$i]['ans2'];
                                $str_check_exam .= ',' . $onlineExamsexamQuestion[$i]['ans2'];
                            }
                        }if (isset($onlineExamsexamQuestion[$i]['Tans3'])) {
                            if (isset($examgradesQuestion[$i]['answer3']) AND $examgradesQuestion[$i]['answer3'] == 'true') {
                                $str_checkType .= ',' . $examgradesQuestion[$i]['ans3'];
                                $str_check_exam .= ',' . $onlineExamsexamQuestion[$i]['ans3'];
                            }
                        }if (isset($onlineExamsexamQuestion[$i]['Tans4'])) {
                            if (isset($examgradesQuestion[$i]['answer4']) AND $examgradesQuestion[$i]['answer4'] == 'true') {
                                $str_checkType .=',' . $examgradesQuestion[$i]['ans4'];
                                $str_check_exam .= ',' . $onlineExamsexamQuestion[$i]['ans4'];
                            }
                        }
                        if ($str_check_exam == $str_checkType) {
                            if (isset($onlineExamsexamQuestion[$i]['questionMark'])) {
                                $sum+= $onlineExamsexamQuestion[$i]['questionMark'];
                            } else {
                                $sum+= 1;
                            }
                        } else {
                            $sum+= 0;
                        }
                    }
                    if (isset($examgradesQuestion[$i]['type']) AND $examgradesQuestion[$i]['type'] == 'text') {
                        if ($onlineExamsexamQuestion[$i]['ans1'] == $examgradesQuestion[$i]['answer']) {
                            if (isset($onlineExamsexamQuestion[$i]['questionMark'])) {
                                $sum+= $onlineExamsexamQuestion[$i]['questionMark'];
                            } else {
                                $sum+= 1;
                            }
                        } else {
                            $sum+= 0;
                        }
                    }
                }
                $average = $sum / $mark;
                $toReturn['questions'][$i]= array('avg'=>$average);
            }
        }
        for ($i = 0; $i < count($examsection); $i++) {
            if (isset($sectionsArray[$examsection[$i]])) {
                $classname = 'Class ' . $sectionsArray[$examsection[$i]];
                $toReturn['final'][$i] = $classname;
            }
        }
        // question average

        return $toReturn;
    }

}
